import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import store from '../../store';
import Form from '../form';
import styles from './styles.css';

const App = () => {
    return (<Provider store={store}>
      <section className='mainWrapper'>
        <header className="appHeader">
          <h1>Exchange project</h1>
        </header>
        <Form />
      </section>
      </Provider>
    );
}

export default App;
