import React from 'react';
import { Provider } from 'react-redux';
import store from '../../store';
import { mount } from 'enzyme';
import Input from './index';

describe('Input component', () => {
  let wrapper;
  it('It should render the Input component', () => {
    const props = {
      type: 'base',
      onChangeCurrency: jest.fn, 
      data: {
        code: 'EUR',
        value: 10,
        error: false,
      },
      onChange: jest.fn, 
    };
    wrapper = mount(
      <Provider store={store}>
        <Input {...props} />
      </Provider>
    );
    expect(wrapper.find('.select')).toHaveLength(1);
    expect(wrapper.find('.input')).toHaveLength(1);
    expect(wrapper.find('.inputError')).toHaveLength(0);
  });
  it('It should render the error paragraph when data.error is true', () => {
    const props = {
      type: 'base',
      onChangeCurrency: jest.fn, 
      data: {
        code: 'EUR',
        value: 10,
        error: true,
      },
      onChange: jest.fn, 
    };
    wrapper = mount(
      <Provider store={store}>
        <Input {...props} />
      </Provider>
    );
    expect(wrapper.find('.inputError')).toHaveLength(1);
  });
});
