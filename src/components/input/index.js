import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import symbols from '../../constants/currencySymbols';
import styles from './styles.css';

/**
 * this function renders the layout on input
 * @param {object} properties 
 */
const Input = ({ data, onChange, pockets, type, onChangeCurrency }) => {
  const error = <p className='inputError'>only 2 digits after the floating point is allowed</p>;
  const { code } = data;
  return (<Fragment>
    <div className='inputWrapper'>
      <label className='inputLabel'>
        <select className='select' value={code} onChange={value => onChangeCurrency(value, type)}>
          { 
            Object
              .keys(symbols)
              .map(item => <option key={item} value={item}>{item}</option>)
          }
        </select>
        <span>
          Balance: {pockets[code]} {symbols[code]}
        </span>
      </label>
      <input
        className='input'
        value={data.value}
        onChange={onChange}
        type='text'
      />
    </div>

    {
      data.error ? error : null
    }
  </Fragment>)
};

export default connect(state => ({pockets: state}), {})(Input);
