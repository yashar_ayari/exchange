import React from 'react';
import { Provider } from 'react-redux';
import store from '../../store';
import { mount } from 'enzyme';
import Form from './index';

jest.mock('../../utils/api');
describe('Form component', () => {
  let wrapper;
  it('It should render 2 inputs and 2 selects', (done) => {
    wrapper = mount(
      <Provider store={store}>
        <Form />
      </Provider>
    );
    expect(wrapper.find('.input')).toHaveLength(2);
    done();
  });
  it('Changing the base currency value should update the value of targetCurrency', async(done) => {
    wrapper = await mount(
      <Provider store={store}>
          <Form />
        </Provider>
      );
      wrapper.update();
      const inputs = await wrapper.find('input');
      inputs.at(0).simulate('change', { target: { value: 10 } });
      expect(wrapper.find('Form').state('targetCurrency').value).toEqual('8.77');
      done();
  });
  it('Changing the target currency value should update the value of baseCurrency', async(done) => {
    wrapper = await mount(
      <Provider store={store}>
          <Form />
        </Provider>
      );
      wrapper.update();
      const inputs = await wrapper.find('input');
      inputs.at(1).simulate('change', { target: { value: 8.77 } });
      expect(wrapper.find('Form').state('baseCurrency').value).toEqual('10.00');
      done();
  });
  it('Entering a number with more than 2 numbers after the floating point in base currency input should throw an error', async(done) => {
    wrapper = await mount(
      <Provider store={store}>
          <Form />
        </Provider>
      );
      wrapper.update();
      const inputs = await wrapper.find('input');
      inputs.at(0).simulate('change', { target: { value: 8.7789 } });
      expect(wrapper.find('Form').state('baseCurrency').error).toEqual(true);
      expect(wrapper.find('Form').state('targetCurrency').value).toEqual(0);
      done();
  });
  it('Entering a number with more than 2 numbers after the floating point in target currency input should throw an error', async(done) => {
    wrapper = await mount(
      <Provider store={store}>
          <Form />
        </Provider>
      );
      wrapper.update();
      const inputs = await wrapper.find('input');
      inputs.at(1).simulate('change', { target: { value: 8.7789 } });
      expect(wrapper.find('Form').state('targetCurrency').error).toEqual(true);
      expect(wrapper.find('Form').state('baseCurrency').value).toEqual(0);
      done();
  });
  it('Changing the base currency code should base and target currency values', async(done) => {
    wrapper = await mount(
      <Provider store={store}>
          <Form />
        </Provider>
      );
      wrapper.update();
      const selects = await wrapper.find('select');
      selects.at(0).simulate('change', { target: { value: 'GHP' } });
      expect(wrapper.find('Form').state('baseCurrency').code).toEqual('GHP');
      expect(wrapper.find('Form').state('baseCurrency').value).toEqual(0);
      expect(wrapper.find('Form').state('targetCurrency').value).toEqual(0);
      done();
  });
  it('Changing the target currency code should base and target currency values', async(done) => {
    wrapper = await mount(
      <Provider store={store}>
          <Form />
        </Provider>
      );
      wrapper.update();
      const selects = await wrapper.find('select');
      selects.at(1).simulate('change', { target: { value: 'USD' } });
      expect(wrapper.find('Form').state('targetCurrency').code).toEqual('USD');
      expect(wrapper.find('Form').state('baseCurrency').value).toEqual(0);
      expect(wrapper.find('Form').state('targetCurrency').value).toEqual(0);
      done();
  });
});
