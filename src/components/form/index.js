import React from 'react';
import { connect } from 'react-redux';
import { currencyConvertor } from '../../utils/convertor';
import Input from '../input';
import { updatePockets } from '../../store/actions';
import { getExchangeRates } from '../../utils/api';
import styles from './styles.css';

const inputValidationPattern = /^\d*(\.\d\d?)?$/;
class Form extends React.Component {
  state = {
    baseCurrency: {
      code: 'EUR',
      value: 0,
      error: false,
    },
    targetCurrency: {
      code: 'GBP',
      value: 0,
      error: false,
    },
    rates: {},
  }

  /**
   * update the value of the base currency input and when there is no error update 
   * the value of target currency input
   */
  updateBaseData = ({ target }) => {
    const { baseCurrency, targetCurrency, rates } = this.state;
    const error = !inputValidationPattern.test(target.value);
    this.setState({
      baseCurrency: {
        code: baseCurrency.code,
        value: target.value,
        error,
      },
    });
    if (!error) {
      this.setState({
        targetCurrency: {
          code: targetCurrency.code,
          value: currencyConvertor(baseCurrency.code, targetCurrency.code, target.value, rates),
        }, 
      });
    }
  }


  /**
   * update the value of the target currency input and when there is no error update 
   * the value of base currency input
   */
  updateTargetData = ({ target }) => {
    const { baseCurrency, targetCurrency, rates } = this.state;
    const error = !inputValidationPattern.test(target.value);
    this.setState({
      targetCurrency: {
        code: targetCurrency.code,
        value: target.value,
        error,
      },
    });
    if (!error) {
      this.setState({
        baseCurrency: {
          code: baseCurrency.code,
          value: currencyConvertor(targetCurrency.code, baseCurrency.code, target.value, rates),
        }, 
      });
    }
  }

  /**
   * update base and target currency pockets 
   */
  exchange = () => {
    const { baseCurrency, targetCurrency } = this.state;
    const { updatePockets, pockets } = this.props;
    updatePockets({
      [baseCurrency.code]: pockets[baseCurrency.code] - baseCurrency.value,
      [targetCurrency.code]: pockets[targetCurrency.code] + targetCurrency.value*1,
    });
  }

  changeCurrency = ({ target }, type) => {
    const { baseCurrency, targetCurrency } = this.state;
    if (type === 'base') {
      this.setState({
        baseCurrency: {
          code: target.value,
          value: 0,
        },
        targetCurrency: {
          code: targetCurrency.code,
          value: 0,
        }
      });
    } else {
      this.setState({
        targetCurrency: {
          code: target.value,
          value: 0,
        },
        baseCurrency: {
          code: baseCurrency.code,
          value: 0,
        }
      });
    }
  }

  componentDidMount() {
    getExchangeRates().then((data) => {
      this.setState({
        rates: data.rates,
      });
      this.interval = setInterval(() => {
        getExchangeRates().then((data) => {
          this.setState({
            rates: data.rates,
          });
        });
      }, 10000);
    });
  }

  componentDidUnMount() {
    clearInterval(this.interval);
  }

  render() {
    const { baseCurrency, targetCurrency } = this.state;
    return (
      <div>
        <Input
          type='base'
          onChangeCurrency={this.changeCurrency}
          data={baseCurrency}
          onChange={this.updateBaseData}
        />
        <Input
          type='target'
          onChangeCurrency={this.changeCurrency}
          data={targetCurrency}
          onChange={this.updateTargetData}
        />
        <button
          className='exchangeButton'
          disabled={!inputValidationPattern.test(baseCurrency.value) ||
            !inputValidationPattern.test(targetCurrency.value)}
          onClick={this.exchange}>
          Exchange
        </button>
      </div>
    )
  }
}

export default connect(
    state => ({pockets: state}),
    { updatePockets },
  )(Form);
