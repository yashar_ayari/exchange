
import actions from '../constants/actions';

const initialState = {
  EUR: 50,
  USD: 50,
  GBP: 40,
};
/**
 * update pockets status base on the upcoming action
 * @param {object} state 
 * @param {object} action 
 */
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actions.updatePockets:
      return Object.assign({}, state, action.data);
    default:
      return state
  }
}

