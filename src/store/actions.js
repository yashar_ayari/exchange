import actionTypes from '../constants/actions';

/**
 * An action to dispatch update pockets
 * @param {object} data
 */
export const updatePockets = data => ({
  data,
  type: actionTypes.updatePockets,
});
