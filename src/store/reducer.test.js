import reducer from './reducer';
import actions from '../constants/actions';

describe('pocket reducer', () => {
  const initialState = {
    EUR: 50,
    USD: 50,
    GBP: 40,
  };
  it('it should return the initial state value when it called without any arguments', () => {
    expect(reducer()).toEqual(initialState);
  })
  it('it should return the padateed state value when it called with pdatePockets action', () => {
    const action = {
      type: actions.updatePockets,
      data: {
        EUR: 80,
        USD: 150,
      }
    };
    const expectedValue = {
      EUR: 80,
      USD: 150,
      GBP: 40,
    };
    expect(reducer(initialState, action)).toEqual(expectedValue);
  })
})
