/**
 * convert other currencies to US dollar
 * @param {string} targetCurrency
 * @param {number} amount
 * @param {object} rates
 * @returns {number}
 */
const convertToUSD = (targetCurrency, amount, rates) => {
  return rates[targetCurrency] * amount;
}

/**
 * convert other US dollar to currencies
 * @param {string} targetCurrency
 * @param {number} amount
 * @param {object} rates
 * @returns {number}
 */
const convertFromUSD = (targetCurrency, amount, rates) => {
  return amount / rates[targetCurrency];
}
/**
 * convert other currencies
 * @param {string} targetCurrency
 * @param {number} amount
 * @param {object} rates
 * @returns {number}
 */
export const currencyConvertor = (baseCurrency, targetCurrency, amount, rates) => {
  const valueInUsd = convertToUSD(targetCurrency, amount, rates);
  return convertFromUSD(baseCurrency, valueInUsd, rates).toFixed(2);
}
