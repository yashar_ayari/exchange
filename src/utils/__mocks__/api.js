import data from '../../constants/currencyRatesMock';

export const getExchangeRates = () => new Promise((resolve) => {
  resolve(data)
});
