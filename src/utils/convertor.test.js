import { currencyConvertor } from './convertor';
import mockData from '../constants/currencyRatesMock';

describe('currency convertor', () => {
  it('it shoudld convert EUR to GHP', () => {
    expect(currencyConvertor('EUR', 'GBP', 10, mockData.rates)).toBe('8.77');
  })
  it('it shoudld convert GBP to EUR', () => {
    expect(currencyConvertor('GBP', 'EUR', 10, mockData.rates)).toBe('11.41');
  })
})
