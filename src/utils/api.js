
/**
 * fetch data from openexchangerates api
 */
export const getExchangeRates = () =>
  fetch('https://openexchangerates.org/api/latest.json?app_id=b1ae6368a92749a69ec41dc6c9e9911f')
  .then((response) => {
    return response.json();
  });
